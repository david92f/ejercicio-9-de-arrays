/**
 * 
 */
package Cine;

/**
 * @author Programacion
 *
 */
public class Espectador {

	private String nombre;
	private int edad;

	/**
	 * @param nombre
	 * @param edad
	 */
	public Espectador() {
		this.nombre = generarNombre();
		this.edad = generarEdad();
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the edad
	 */
	public int getEdad() {
		return edad;
	}

	/**
	 * @param edad the edad to set
	 */
	public void setEdad(int edad) {
		this.edad = edad;
	}

	// generas una edad aleatorio del 5 al 95
	public int generarEdad() {
		return (int) ((Math.random() * 90) + 5);
	}

	// generas un nombre aleatorio con mayusculas
	public String generarNombre() {
		String nombre = "";
		for (int i = 0; i < 5; i++) {
			// passar de double a char entre valor de 33 a 126, posteriormente se concatena
			// a un string
			nombre += (char) (Math.random() * 25 + 65);
		}
		return nombre;
	}
}
