/**
 * 
 */
package Cine;

/**
 * @author Programacion
 *
 */
public class CineApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// variables
		int edadMinima = 7, precioFinal = 9;
		// generaramos la sala y la tabla Espectador
		Pelicula p = new Pelicula("Avengers: End Game", 180, edadMinima, "Rusos");
		Sala sala0 = new Sala(precioFinal, p.getTitulo());
		Espectador[][] espectadores = sala0.generarEspectadores(50, edadMinima);
		// MUESTRO LOS DATOS DE LA PELICULA
		System.out.println("Nombre de la pelicula: " + sala0.getTituloPelicula() + "\nPrecio de la entrada: "
				+ sala0.getPrecio() + " �\nEdad m�nima: " + edadMinima + " a�os");
		// bucle que que muestra la tabla Espectador[9][8]
		for (int i = 0; i < espectadores.length; i++) {
			for (int j = 0; j < espectadores[0].length; j++) {
				// hacemos un try catch para evitar error de null pointer
				try {
					System.out.print(espectadores[i][j].getNombre() + "\t");
				} catch (NullPointerException e) {
					System.out.print("-----\t");
				}
			}
			System.out.println();
		}

	}

}
