/**
 * 
 */
package Cine;

/**
 * @author Programacion
 *
 */
public class Pelicula {

	protected String titulo;
	protected int duracion;
	protected int edad_min;
	protected String director;
	/**
	 * @param titulo
	 * @param duracion
	 * @param edad_min
	 * @param director
	 */
	public Pelicula(String titulo, int duracion, int edad_min, String director) {
		this.titulo = titulo;
		this.duracion = duracion;
		this.edad_min = edad_min;
		this.director = director;
	}
	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}
	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	/**
	 * @return the duracion
	 */
	public int getDuracion() {
		return duracion;
	}
	/**
	 * @param duracion the duracion to set
	 */
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	/**
	 * @return the edad_min
	 */
	public int getEdad_min() {
		return edad_min;
	}
	/**
	 * @param edad_min the edad_min to set
	 */
	public void setEdad_min(int edad_min) {
		this.edad_min = edad_min;
	}
	/**
	 * @return the director
	 */
	public String getDirector() {
		return director;
	}
	/**
	 * @param director the director to set
	 */
	public void setDirector(String director) {
		this.director = director;
	}
	@Override
	public String toString() {
		return "Pelicula [titulo=" + titulo + ", duracion=" + duracion + ", edad_min=" + edad_min + ", director="
				+ director + "]";
	}
}
