package Cine;

public class Sala {

	private int precio;
	private String tituloPelicula;

	/**
	 * @param precio
	 * @param tituloPelicula
	 */
	public Sala(int precio, String tituloPelicula) {
		this.precio = precio;
		this.tituloPelicula = tituloPelicula;
	}

	/**
	 * @return the tituloPelicula
	 */
	public String getTituloPelicula() {
		return tituloPelicula;
	}

	/**
	 * @param tituloPelicula the tituloPelicula to set
	 */
	public void setTituloPelicula(String tituloPelicula) {
		this.tituloPelicula = tituloPelicula;
	}

	/**
	 * @return the precio
	 */
	public int getPrecio() {
		return precio;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(int precio) {
		this.precio = precio;
	}

	// metodos
	// crea una tabla de 9x8 espectadores con n espectadores
	public Espectador[][] generarEspectadores(int numeroEspectadores,int edadMinima) {
		// variables
		int x, y;
		Espectador[][] espectadores = new Espectador[9][8];
		// para evitar que siga el while en un bucle infinitos
		if (numeroEspectadores > 72) {
			numeroEspectadores = 72;
		}
		//generar tabla con valores por defecto
//		Arrays.fill(Espectador[], new Espectador());
		// bucle para generar n espectadores
		for (int i = 0; i < numeroEspectadores; i++) {
			// bucle while en que no para hasta que encuentre una posicion en la tabla libre
			// (null)
			do {
				x = generarAleatorio(9);
				y = generarAleatorio(8);
				Espectador e=new Espectador();
				//comprobamos la edad
				if (e.getEdad()>=edadMinima) {
					espectadores[x][y] = e;
				}
				
			} while (espectadores[x][y] == null);
		}
		return espectadores;
	}

	private int generarAleatorio(int n) {
		return (int) (Math.random() * n);
	}

}
