/**
 * 
 */
package Ej10;

import java.util.Arrays;
import javax.swing.JOptionPane;

/**
 * @author David
 *
 *         10) Crea un array de n�meros de un tama�o pasado por teclado, el
 *         array contendr� n�meros aleatorios primos entre los n�meros deseados,
 *         por �ltimo nos indicar cual es el mayor de todos. Haz un m�todo para
 *         comprobar que el n�mero aleatorio es primo, puedes hacer todos lo
 *         m�todos que necesites.
 */
public class Ej10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// variables
		int n, r = 99;
		int array[];
		// pides por teclado
		n = Integer.parseInt(JOptionPane.showInputDialog("Introduce el tama�o del array"));
		array = new int[n];
		for (int i = 0; i < array.length; i++) {
			// generar un numero primo del 2 al 99
			array[i] = generarPrimo(r);
		}
		// imprimes el array de aleatorios generado
		System.out.println(Arrays.toString(array));
		n = primoMayor(array);
		// ventana informativa del rango y el mayor numero primo encontado
		JOptionPane.showMessageDialog(null, "El numero primo mayor generado en un rango del 2 al " + r + " es el " + n);
	}

	// metodos
	// generar un numero primo dentro del rango de 2 a n
	private static int generarPrimo(int numero) {
		int n = 2;
		boolean esPrimo;
		// buscas hasta encontrar un numero primo aleatorio
		do {
			// generas un numero que almenos sea igual o mayor que 2, pero no supera a
			// numero
			n = (int) (Math.random() * (numero - 2) + 2);
			// considero que inicialmente es un primo
			esPrimo = true;
			for (int i = 2; i <= n / 2 && esPrimo; i++) {				// comprueba si es primo
				if (n % i == 0) {
					// si se cumple la condicion es que no es primo
					esPrimo = false;
				}
			}
		} while (!esPrimo);

		return n;
	}

	// comparar de uno en uno para saber cual es el numero mayor
	private static int primoMayor(int[] ale) {
		int mayor = ale[0];
		// usa el primero para poder a compararlo con el resto
		for (int i = 1; i < ale.length; i++) {
			// si alguno es mayor que el que esta guardado se sustituye
			if (ale[i] > mayor) {
				mayor = ale[i];
			}
		}
		return mayor;
	}

}
