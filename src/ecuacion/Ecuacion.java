/**
 * 
 */
package ecuacion;

/**
 * @author Programacion
 *
 */
public class Ecuacion {

	private int a;
	private int b;
	private int c;

	// variables para el calculo
	private double n, r;

	/**
	 * @param a
	 * @param b
	 * @param c
	 */
	public Ecuacion(int a, int b, int c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	// calculo de las dos raices y muestra su resultado
	public void obtenerRaices() {
		r = (-b + Math.sqrt(n)) / (2 * a);
		// imprimir el resultado con printf
		System.out.printf("La primera soluci�n es %.2f%n", r);
		r = (-b - Math.sqrt(n)) / (2 * a);
		System.out.printf("La segunda soluci�n es %.2f%n", r);
	}

	public void obtenerRaiz() {
		r = -b / (2 * a);
		System.out.println("La �nica soluci�n es " + r);
	}

	// devuelve el calculo que hay dentro la raiz de la ecuacion de segundo grado
	public double getDescriminate() {
		return Math.pow(b, 2) - (4 * a * c);
	}

	// segun n sabes si tiene dos soluciones (raices) la ecuacion
	public boolean tieneRaices() {
		if (n > 0) {
			return true;
		} else {
			return false;
		}
	}

	// segun n sabes si tiene una solucion (raiz) la ecuacion
	public boolean tieneRaiz() {
		if (n == 0) {
			return true;
		} else {
			return false;
		}
	}

	// hacer el calculo de ecuacion de segundo grado
	public void calcular() {
		// calculo de dentro de la raiz cuadrada
		n = getDescriminate();
		// comprobar si hay 2, 1 o 0 soluciones
		if (tieneRaices()) {
			obtenerRaices();
		} else if (tieneRaiz()) {
			obtenerRaiz();
		} else {
			System.out.println("No tiene soluci�n.");
		}
	}
}
