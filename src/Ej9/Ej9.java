package Ej9;

import javax.swing.JOptionPane;

public class Ej9 {

	// variable global
	static int[] aleatorios;

	public static void main(String[] args) {
		// variables
		int n;

		// pedir por teclado el tama�o del array
		n = Integer.parseInt(JOptionPane.showInputDialog("Introduce el tama�o del array"));

		// declarar el tama�o del array
		aleatorios = new int[n];

		// rellenar array con un metodo
		fillArray();

		// mostrar los valores del array en un metodo
		mostrarContenido();

	}

	// metodos

	// mostrar el contenido del array
	private static void mostrarContenido() {
		int suma=0;
		String cadena="";
		//bucle que consulta todos los valores del array menos el ultimo, los muestras y haces la suma
		for (int i = 0; i < aleatorios.length-1; i++) {
			cadena+=aleatorios[i] + " + ";
			suma+=aleatorios[i];
		}
		//contemplamos el ultimo valor del array y lo sumamos a la suma
		suma+=aleatorios[aleatorios.length-1];
		//contemplamos el ultimo valor del array y lo a�adimos a la cadena y mostramos el resultado de la suma
		cadena += aleatorios[aleatorios.length-1] + " = " + suma;
		JOptionPane.showMessageDialog(null, cadena);
	}

	//metodo pars rellenar el array de numeros aleatorios
	private static void fillArray() {
		for (int i = 0; i < aleatorios.length; i++) {
			// llamamos al metodo generar aleatorios de 0 al 9
			aleatorios[i] = generarRandom();
		}

	}

	// metodo para generar el numero aleatorio pra cada posicion del array
	private static int generarRandom() {
		return (int) (Math.random() * 10);
	}

}
