/**
 * 
 */
package Ej11;

import javax.swing.JOptionPane;

/**
 * @author david
 *
 *         11) Crea dos arrays de n�meros con una posici�n pasado por teclado.
 *         Uno de ellos estar� rellenado con n�meros aleatorios y el otro
 *         apuntara al array anterior, despu�s crea un nuevo array con el primer
 *         array (usa de nuevo new con el primer array) con el mismo tama�o que
 *         se ha pasado por teclado, rellenalo de nuevo con n�meros aleatorios.
 *         Despu�s, crea un m�todo que tenga como par�metros, los dos arrays y
 *         devuelva uno nuevo con la multiplicaci�n de la posici�n 0 del array1
 *         con el del array2 y as� sucesivamente. Por �ltimo, muestra el
 *         contenido de cada array.
 */
public class ej11 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// 2 arrays donde se gurdara n numeros aletorios
		int aleatorios1[], aleatorios2[];
		// array donde se guardara la multiplicaciones de los 2 anteriores
		int multipliciones[];

		// pide por teclado
		int n = Integer.parseInt(JOptionPane.showInputDialog("Introduce el tama�o de los 2 arrays"));

		// dar tama�o a los arrays
		aleatorios1 = new int[n];
		aleatorios2 = new int[n];
		multipliciones = new int[n];

		// llamas al metodo para rellenar los arrys
		fillArray(aleatorios1);
		fillArray(aleatorios2);

		multipliciones = multplicarArrays(aleatorios1, aleatorios2);
		// mostrar los 3 arrays
		mostrarArrays(aleatorios1, aleatorios2, multipliciones);

	}

	// metodos privados
	private static void mostrarArrays(int[] aleatorios1, int[] aleatorios2, int[] multipliciones) {
		for (int i = 0; i < aleatorios1.length; i++) {
			System.out.print(aleatorios1[i] + "\t");
		}
		System.out.println();
		for (int i = 0; i < aleatorios2.length; i++) {
			System.out.print(aleatorios2[i] + "\t");
		}
		System.out.println();
		for (int i = 0; i < multipliciones.length; i++) {
			System.out.print(multipliciones[i] + "\t");
		}
	}

	// metodo pars rellenar el array de numeros aleatorios
	private static void fillArray(int[] aleatorios) {
		for (int i = 0; i < aleatorios.length; i++) {
			// llamamos al metodo generar aleatorios de 0 al 9
			aleatorios[i] = generarRandom();
		}

	}

	// metodo para generar el numero aleatorio pra cada posicion del array
	private static int generarRandom() {
		return (int) (Math.random() * 10);
	}

	// mulplica las mismas posiciones de los 2 arrays primero y guarda el resultado
	// en el tercero
	private static int[] multplicarArrays(int[] aleatorios1, int[] aleatorios2) {
		int multipliciones[] = new int[aleatorios1.length];
		for (int i = 0; i < multipliciones.length; i++) {
			multipliciones[i] = aleatorios1[i] * aleatorios2[i];
		}
		return multipliciones;
	}
}
