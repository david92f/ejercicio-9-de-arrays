/**
 * 
 */
package Libro;

/**
 * @author Programacion
 *
 */
public class Libro {

	private String isbn;
	private String titulo;
	private String autor;
	private int nPag;
	/**
	 * @param iSBN
	 * @param titulo
	 * @param autor
	 * @param nPag
	 */
	public Libro(String iSBN, String titulo, String autor, int nPag) {
		isbn = iSBN;
		this.titulo = titulo;
		this.autor = autor;
		this.nPag = nPag;
	}
	
	/**
	 * @return the isbn
	 */
	public String getIsbn() {
		return isbn;
	}

	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * @return the autor
	 */
	public String getAutor() {
		return autor;
	}

	/**
	 * @param autor the autor to set
	 */
	public void setAutor(String autor) {
		this.autor = autor;
	}

	/**
	 * @return the nPag
	 */
	public int getnPag() {
		return nPag;
	}

	/**
	 * @param nPag the nPag to set
	 */
	public void setnPag(int nPag) {
		this.nPag = nPag;
	}

	@Override
	public String toString() {
		return "El libro con " + isbn + " creado por el autor " + autor + " tiene " + nPag + " p�ginas.";
	}
	
	
}
