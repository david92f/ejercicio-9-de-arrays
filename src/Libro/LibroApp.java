/**
 * 
 */
package Libro;

/**
 * @author Programacion
 *
 */
public class LibroApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// 2 objetos Libro
		Libro l1=new Libro("1772DFGH", "Los pilares de la tierra", "Kent Follet", 1068);
		Libro l2=new Libro("5454BSHY", "La caida de los gigantes", "Kent Follet", 1024);
		//muestras los datos del tostring
		System.out.println(l1);
		System.out.println(l2);
		// miramos que el libro que tiene mas paginas, en el else l1 es igual o menor que l2
		if (l1.getnPag()>l2.getnPag()) {
			System.out.println("El libro con m�s p�ginas es "+ l1.getTitulo());
		}else {
			System.out.println("El libro con m�s p�ginas es "+ l2.getTitulo());
		}

	}

}
