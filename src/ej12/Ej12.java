/**
 * 
 */
package ej12;

import java.util.Arrays;

import javax.swing.JOptionPane;

/**
 * @author david
 *
 *         12) Crea un array de n�meros de un tama�o pasado por teclado, el
 *         array contendr� n�meros aleatorios entre 1 y 300 y mostrar aquellos
 *         n�meros que acaben en un d�gito que nosotros le indiquemos por
 *         teclado (debes controlar que se introduce un numero correcto), estos
 *         deben guardarse en un nuevo array. Por ejemplo, en un array de 10
 *         posiciones e indicamos mostrar los n�meros acabados en 5, podr�a
 *         salir 155, 25, etc.
 */
public class Ej12 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// variables
		int n, digito, nAcabados, array[], arrayFinal[];

		// teclado
		try {
			n = Integer.parseInt(JOptionPane
					.showInputDialog("Introduce el tama�o del array de" + " numeros aleatorios" + " del 1 al 300"));
		} catch (Exception e) {
			// por defecto el array sera de 2 si no introduce un numero
			n = 2;
		}
		// controla que pongas un digito por teclado y lo guardas
		digito = controlarDigito();

		array = generarArray(n);

		nAcabados = contarNumerosAcabadosEn(array, digito);

		arrayFinal = generarArrayAcabadoEnDigito(array, nAcabados, digito);

		JOptionPane.showMessageDialog(null, Arrays.toString(arrayFinal));
	}

	// metodos
	private static int[] generarArrayAcabadoEnDigito(int[] array, int nAcabados, int digito) {
		int numAcabados[] = new int[nAcabados];
		String numero;
		int n, i = 0;
		for (int a : array) {
			// compruebas que el ultimo digito del numero del array i y el parametro digito
			// son iguales, si es asi incrementas contador
			numero = String.valueOf(a);
			numero = String.valueOf(numero.charAt(numero.length() - 1));
			n = Integer.parseInt(numero);
			// assignas a cada posicion los numeros acabados en el digito corresponidente
			if (n == digito) {
				numAcabados[i] = a;
				i++;
			}
		}
		return numAcabados;
	}

	private static int contarNumerosAcabadosEn(int[] array, int digito) {
		int contador = 0, n;
		String numero;
		for (int i : array) {
			// compruebas que el ultimo digito del numero del array i y el parametro digito
			// son iguales, si es asi incrementas contador
			numero = String.valueOf(i);
			numero = String.valueOf(numero.charAt(numero.length() - 1));
			n = Integer.parseInt(numero);
			if (n == digito) {
				contador++;
			}
		}
		return contador;
	}

	private static int[] generarArray(int n) {
		// variables
		int[] aleatorios = new int[n];
		// bucle foreach donde generas n numeros aleatorios del 1 al 300 y lo guardas en
		// un array
		for (int i = 0; i < aleatorios.length; i++) {
			aleatorios[i] = (int) (Math.random() * 300 + 1);
		}
		return aleatorios;
	}

	// introducir el digito en el cual acabara los numero que se mostraran por
	// pantalla
	private static int controlarDigito() {
		int digito;
		do {
			try {
				digito = Integer.parseInt(JOptionPane.showInputDialog("Indica el digito (0-9) que quieres para "
						+ "mostrar los numeros que acaben en el digito introducido"));
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Error. Introduce un digito (0-9)");
				digito = -1;
			}
			// mientras el resultado de la divisi�n de 10 entre el valor introducido no sea
			// de 0-9 no saldra del bucle while
		} while (digito / 10 != 0 || digito < 0);
		return digito;
	}

}
